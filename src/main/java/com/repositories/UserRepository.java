package com.repositories;

import com.models.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import java.util.List;

@org.springframework.stereotype.Repository
public interface UserRepository extends Repository<User, Long> {
    List<User> findAllBy();

    User findById(Long id);

    void save(User blog);

    void deleteById(Long id);

    @Query(value = "select u.name as name, u.id as id, SUM(b.liked) as liked " +
            "from User u inner join Blog b on u.id = b.user.id GROUP BY u.id, u.name")
    List<Object> getTopLike();
}
