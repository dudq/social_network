package com.repositories;

import com.models.Blog;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import java.util.List;

@org.springframework.stereotype.Repository
public interface BlogRepository extends Repository<Blog, Long> {
    List<Blog> findAllBy();

    Blog findById(Long id);

    void save(Blog blog);

    void deleteById(Long id);

    @Query("select b from Blog b where b.user.id=?1")
    List<Blog> findBlogsByUserId(Long id);

    @Query("select b from Blog b where b.user.id <> ?1 and b.content like %?2%")
    List<Blog> receiveBlogsByContent(Long id, String content);

    @Query("select b from Blog b where b.user.id <> ?1 and b.user.name like %?2%")
    List<Blog> receiveBlogsByUserName(Long id, String userName);

}
