package com.controllers;

import com.models.Blog;
import com.models.DTO.BlogRequestBody;
import com.services.BlogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Controller
public class BlogController {
    @Autowired
    private BlogService blogService;

    @GetMapping(value = "/blogs")
    public String listAllBlogs(Model model) {
        List<Blog> blogs = blogService.getBlogs();
        if (blogs == null) {
            model.addAttribute("message", "Blog is not exist");
            return "error";
        }
        model.addAttribute("blogs", blogs);
        return "listBlog";
    }

    @GetMapping(value = "/{id}")
    public String getBlog(@PathVariable("id") Long id, Model model) {
        try {
            Blog blog = blogService.getBlog(id);
            model.addAttribute("blog", blog);
            return "view";
        } catch (Exception e) {
            model.addAttribute("message", e.getMessage());
            return "error";
        }
    }

    @GetMapping("/create")
    public String showCreateForm(Model model) {
        Blog blog = new Blog();
        model.addAttribute("blog", blog);
        return "createBlog";
    }

    @PostMapping("/create")
    public String createBlog(@ModelAttribute Blog blog, RedirectAttributes redirectAttributes, Model model) {
        try {
            blogService.saveBlog(blog);
            redirectAttributes.addFlashAttribute("message", "New blog created");
            return "redirect:/blogs";
        } catch (Exception e) {
            model.addAttribute("message", e.getMessage());
            return "error";
        }
    }

    @GetMapping("edit/{id}")
    public String showEditForm(@PathVariable("id") Long id, Model model) {
        Blog blog = blogService.getBlog(id);
        if (blog != null) {
            model.addAttribute("blog", blog);
            return "editBlog";
        }
        model.addAttribute("message", "Blog not exist");
        return "error";
    }

    @PostMapping("edit/{id}")
    public String updateBlog(@ModelAttribute Blog blog, Model model) {
        try {
            blogService.updateBlog(blog);
            model.addAttribute("message", "Blog added successfully");
            return "editBlog";
        } catch (Exception e) {
            model.addAttribute("message", e.getMessage());
            return "error";
        }
    }

    @GetMapping("delete/{id}")
    public String showDeleteForm(@PathVariable("id") Long id, Model model) {
        Blog blog = blogService.getBlog(id);
        if (blog != null) {
            model.addAttribute("blog", blog);
            return "deleteBlog";
        }
        model.addAttribute("message", "Blog not exist");
        return "error";
    }

    @PostMapping("delete/{id}")
    public String deleteBlog(@PathVariable(value = "id") Long id, RedirectAttributes redirectAttributes, Model model) {
        try {
            blogService.deleteBlog(id);
            redirectAttributes.addFlashAttribute("message", "Blog deleted successfully");
            return "redirect:/blogs";
        } catch (Exception e) {
            model.addAttribute("message", e.getMessage());
            return "error";
        }
    }

    @GetMapping("/filter")
    public String receiveBlogsByFilter(@RequestBody BlogRequestBody blogRequestBody) {
        try {
            List<Blog> blogs = blogService.receiveBlogsAnotherUser(blogRequestBody);
            return new ResponseEntity<>(blogs, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/{id}/like")
    public ResponseEntity<Void> updateLikeBlog(@PathVariable(value = "id") Long id) {
        try {
            blogService.updateLikeBlog(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
