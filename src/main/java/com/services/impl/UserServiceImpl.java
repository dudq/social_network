package com.services.impl;

import com.models.Blog;
import com.models.DTO.UserTopLikeResponse;
import com.models.User;
import com.repositories.BlogRepository;
import com.repositories.UserRepository;
import com.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;


public class UserServiceImpl implements UserService {

    private final int NAME = 0;
    private final int ID = 1;
    private final int LIKED = 2;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BlogRepository blogRepository;

    @Override
    public List<User> getUsers() {
        return userRepository.findAllBy();
    }

    @Override
    public User getUser(Long id) {
        return userRepository.findById(id);
    }

    @Override
    public void saveUser(User user) {
        userRepository.save(user);
    }

    @Override
    public void removeUser(Long id) {
        userRepository.deleteById(id);
    }

    @Override
    public List<Blog> getBlogsByUser(Long id) {
        return blogRepository.findBlogsByUserId(id);
    }

    @Override
    public List<UserTopLikeResponse> getTopLike() {
        List<Object> topLike = userRepository.getTopLike();
        int lengthTopLikes = topLike.size();
        Object[] userTopLikeTemp;
        List<UserTopLikeResponse> responses = new ArrayList<>();
        for (int i = 0; i < lengthTopLikes; i++) {
            UserTopLikeResponse userTopLikeResponse = new UserTopLikeResponse();
            userTopLikeTemp = (Object[]) topLike.get(i);
            userTopLikeResponse.setName((String) userTopLikeTemp[NAME]);
            userTopLikeResponse.setId((Long) userTopLikeTemp[ID]);
            userTopLikeResponse.setLiked((Long) userTopLikeTemp[LIKED]);
            responses.add(userTopLikeResponse);
        }
        return responses;
    }
}
