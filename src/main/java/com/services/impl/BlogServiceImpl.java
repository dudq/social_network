package com.services.impl;

import com.common.BlogFilter;
import com.common.exception.InvalidRequestException;
import com.models.Blog;
import com.models.DTO.BlogRequestBody;
import com.repositories.BlogRepository;
import com.services.BlogService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public class BlogServiceImpl implements BlogService {

    @Autowired
    private BlogRepository blogRepository;

    @Override
    public List<Blog> getBlogs() {
        return blogRepository.findAllBy();
    }

    @Override
    public Blog getBlog(Long id) {
        Blog blog = blogRepository.findById(id);
        if (blog == null) {
            throw new InvalidRequestException("Blog is not existed");
        }
        return blog;
    }

    @Override
    public void updateBlog(Blog blog) {
        Blog blogExisting = blogRepository.findById(blog.getId());
        if (blogExisting == null) {
            throw new InvalidRequestException("Blog is not existed");
        }
        blogRepository.save(blog);
    }

    @Override
    public List<Blog> receiveBlogsAnotherUser(BlogRequestBody blogRequestBody) {
        if (BlogFilter.CONTENT.equals(blogRequestBody.getType())) {
            return blogRepository.receiveBlogsByContent(blogRequestBody.getUserId(), blogRequestBody.getCriteria());
        } else if (BlogFilter.NAME.equals(blogRequestBody.getType())) {
            return blogRepository.receiveBlogsByUserName(blogRequestBody.getUserId(), blogRequestBody.getCriteria());
        } else {
            throw new InvalidRequestException("Blog is not found");
        }
    }

    @Override
    public void updateLikeBlog(Long id) {
        Blog blog = blogRepository.findById(id);
        if (blog == null) {
            throw new InvalidRequestException("Blog is not existed");
        }
        blog.setLiked(blog.getLiked() + 1);
        blogRepository.save(blog);
    }

    @Override
    public void saveBlog(Blog blog) {
        blogRepository.save(blog);
    }

    @Override
    public void deleteBlog(Long id) {
        Blog blog = blogRepository.findById(id);
        if (blog == null) {
            throw new InvalidRequestException("Blog is not existed");
        }
        blogRepository.deleteById(id);
    }

}
