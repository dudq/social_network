package com.services;

import com.models.Blog;
import com.models.DTO.UserTopLikeResponse;
import com.models.User;

import java.util.List;

public interface UserService {
    List<User> getUsers();

    User getUser(Long id);

    void saveUser(User user);

    void removeUser(Long id);

    List<Blog> getBlogsByUser(Long id);

    List<UserTopLikeResponse> getTopLike();
}
