package com.services;

import com.models.Blog;
import com.models.DTO.BlogRequestBody;

import java.util.List;

public interface BlogService {
    List<Blog> getBlogs();

    Blog getBlog(Long id);

    void saveBlog(Blog blog);

    void deleteBlog(Long id);

    void updateBlog(Blog blog);

    List<Blog> receiveBlogsAnotherUser(BlogRequestBody blogRequestBody);

    void updateLikeBlog(Long id);
}
